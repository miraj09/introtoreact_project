import React, { Component } from "react";
import ReactDOM from "react-dom";
import s from "./component/app.component.css"
import { Comment, LikeIcon, Like } from "./component/app.component"
class Status extends React.Component {
  render() {
    return (
      <div className="col-6 offset-3">
        <div className="card">
          <div className="card-block">
            <div className="row">
              <div className="col-2">
                <img src="https://zen-of-programming.com/react-intro/selfiesquare.jpg" className={s.profile_pic} />
              </div>
              <div className="col-10 profile-row">
                <div className="row">
                  <a href="#">The Zen of Programming</a>
                </div>
                <div className="row">
                  <small className="post-time">10 mins</small>
                </div>
              </div>
            </div>
            <p>Hello, world!</p>
            <Like />
          </div>
          <div className="card-footer text-muted">
            <Comment maxLetters={140} />
          </div>
        </div>
      </div>
    )
  }
}

// Tells React to attach the HelloWorld component to the 'root' HTML div
ReactDOM.render(<Status />, document.getElementById("root"))
